import uvicorn

from core.settings import DEBUG

if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8000, debug=DEBUG, reload=DEBUG)
