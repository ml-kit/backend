from core.handlers import on_shutdown, on_startup
from core.settings import ALL_METHODS, ALLOWED_HOSTS, DEBUG, PROJECT_NAME
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from starlette.requests import Request
from starlette.responses import PlainTextResponse

app = FastAPI(
    title=PROJECT_NAME, docs_url="/api/docs/", redoc_url="/api/redoc/", debug=DEBUG
)

# app.add_middleware(
#     CORSMiddleware,
#     allow_origins=ALLOWED_HOSTS,
#     allow_credentials=True,
#     allow_methods=["*"],
#     allow_headers=["*"],
# )


@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    if request.method == "OPTIONS":
        response = PlainTextResponse("OK", status_code=200)
        response.headers["Access-Control-Allow-Headers"] = request.headers.get(
            "Access-Control-Request-Headers", "*"
        )
        response.headers["Access-Control-Max-Age"] = "86400"
    else:
        response = await call_next(request)

    response.headers["Access-Control-Allow-Origin"] = "*"
    response.headers["Access-Control-Allow-Credentials"] = "true"
    response.headers["Access-Control-Allow-Methods"] = ", ".join(ALL_METHODS)
    return response


app.add_event_handler("startup", on_startup(app))
app.add_event_handler("shutdown", on_shutdown(app))
