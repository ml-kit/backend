import logging

from core.settings import DATABASE_URL, MAX_CONNECTIONS_COUNT, MIN_CONNECTIONS_COUNT
from db.mongodb import db
from motor.motor_asyncio import AsyncIOMotorClient


async def connect_to_mongo():
    logging.info("Connecting to MongoDB...")
    db.client = AsyncIOMotorClient(
        str(DATABASE_URL),
        maxPoolSize=MAX_CONNECTIONS_COUNT,
        minPoolSize=MIN_CONNECTIONS_COUNT,
    )
    logging.info("Connected!")


async def close_mongo_connection():
    logging.info("Closing connection to MongoDB...")
    db.client.close()
    logging.info("Closed connection！")
