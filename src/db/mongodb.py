from core.settings import (
    DATABASE_NAME,
    DATABASE_URL,
    MAX_CONNECTIONS_COUNT,
    MIN_CONNECTIONS_COUNT,
)
from motor.motor_asyncio import (
    AsyncIOMotorClient,
    AsyncIOMotorCollection,
    AsyncIOMotorDatabase,
)


class DataBase:
    client: AsyncIOMotorClient = None


db = DataBase()
db.client = AsyncIOMotorClient(
    str(DATABASE_URL),
    maxPoolSize=MAX_CONNECTIONS_COUNT,
    minPoolSize=MIN_CONNECTIONS_COUNT,
)


def get_database() -> AsyncIOMotorClient:
    return db.client


def get_curr_database(conn: AsyncIOMotorClient = get_database()) -> AsyncIOMotorDatabase:
    return conn[DATABASE_NAME]


def get_collection(name, conn: AsyncIOMotorClient = get_database()) -> AsyncIOMotorCollection:
    return conn[DATABASE_NAME][name]
