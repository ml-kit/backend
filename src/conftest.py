import pytest
from main import app
from starlette.testclient import TestClient

api_client = TestClient(app)


@pytest.fixture()
def client() -> TestClient:
    return api_client
