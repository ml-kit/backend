from typing import List

from core.aws import S3Helper
from core.charts import preprocess_bar, preprocess_heatmap
from core.dependencies import exist_column_names, own_project_dependency
from core.fields import ObjectIdStr
from core.matplotlib_charts import MatplotlibCharts
from core.settings import USER_COLLECTION_NAME
from db.mongodb import get_collection
from fastapi import (
    APIRouter,
    Depends,
    File,
    Form,
    HTTPException,
    Path,
    Response,
    UploadFile,
)
from models.charts import BaseChartConfig, ChartType
from models.column import ColumnBase
from models.project import Project
from models.user import User
from pydantic.types import conlist
from starlette.background import BackgroundTasks

from .users import fastapi_users_back

router = APIRouter()


@router.post("/", response_model=Project)
async def create_project(
    *,
    user: User = Depends(fastapi_users_back.get_current_active_user),
    name: str = Form(...),
    file: UploadFile = File(...),
    background_tasks: BackgroundTasks,
):

    file_bytes = await file.read()

    project = await Project.create(
        user_id=user.id, project_name=name, file_name=file.filename, file=file_bytes,
    )

    s3 = S3Helper()
    background_tasks.add_task(s3.upload_file, project.get_path_dataset(), file_bytes)

    return project


@router.get("/?", response_model=List[Project])
async def get_projects(
    *, user: User = Depends(fastapi_users_back.get_current_active_user),
):
    my_projects = await Project.filter(owner=user.id)
    return my_projects


@router.get("/{project_id}/?", response_model=Project)
async def detail_project(
    *, project: Project = Depends(own_project_dependency),
):
    return project


@router.delete("/{project_id}/?", response_model=Project)
async def delete_project(
    *,
    user: User = Depends(fastapi_users_back.get_current_active_user),
    project_id: ObjectIdStr = Path(...),
):
    my_project = await Project.find_one_and_delete(_id=project_id, owner=user.id)
    user_collection = get_collection(USER_COLLECTION_NAME)
    await user_collection.update_one(
        {"id": user.id}, {"$pull": {"projects": my_project.id}}
    )
    return my_project


@router.patch("/{project_id}/columns/?")
async def settings_columns(
    *,
    columns: conlist(ColumnBase, min_items=1),
    project: Project = Depends(exist_column_names),
):
    new_col_dict = {coll.name: coll for coll in columns}

    for column in project.columns:
        if column.name in new_col_dict:
            new_coll = new_col_dict[column.name]
            column.__dict__.update(**new_coll.__dict__)

    to_update = project.dict(by_alias=True, include={"columns"})
    await project.update_model(**to_update)
    return "OK"


@router.post("/{project_id}/charts/?")
async def preprocess_for_charts(
    *, chart_config: BaseChartConfig, project: Project = Depends(own_project_dependency)
):
    if chart_config.chart_type == ChartType.BAR:
        response = await preprocess_bar(project, chart_config)
    elif chart_config.chart_type == ChartType.HEATMAP:
        response = await preprocess_heatmap(project, chart_config)
    elif chart_config.chart_type == ChartType.DISTRIBUTION:
        data = await MatplotlibCharts().distribution_chart(project, chart_config)
        response = Response(content=data)
    elif chart_config.chart_type == ChartType.CATPLOT:
        data = await MatplotlibCharts().cat_plot(project, chart_config)
        response = Response(content=data)
    else:
        raise HTTPException(status_code=400, detail="Not known chart type")
    return response
