from fastapi import APIRouter
from starlette.responses import Response

from core.aws import S3Helper
from core.settings import SSL_KEY_PATH_NAME

router = APIRouter()


@router.get("/test/?")
@router.post("/test/?")
@router.delete("/test/?")
@router.patch("/test/?")
@router.put("/test/?")
async def health_check():
    return {"message": "Hello World"}


async def health_check_ok():
    """
    For aws. Return ok.
    :return:
    """
    return "OK"


async def ssl_certificate():
    content = S3Helper().download_file(SSL_KEY_PATH_NAME)
    return Response(content=content, media_type="text/txt")

