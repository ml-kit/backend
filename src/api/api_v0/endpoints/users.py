from core.settings import AUTH_BACKENDS, DATABASE_NAME, SECRET, USER_COLLECTION_NAME
from db.mongodb import get_database
from fastapi_users import FastAPIUsers
from fastapi_users.db import MongoDBUserDatabase
from models.user import User, UserCreate, UserDB, UserUpdate
from starlette.requests import Request

client = get_database()
collection = client[DATABASE_NAME][USER_COLLECTION_NAME]
user_db = MongoDBUserDatabase(UserDB, collection)

fastapi_users_back = FastAPIUsers(
    user_db, AUTH_BACKENDS, User, UserCreate, UserUpdate, UserDB, SECRET,
)

router = fastapi_users_back.router


@fastapi_users_back.on_after_register()
def on_after_register(user: User, request: Request):
    print(f"User {user.id} has registered.")


@fastapi_users_back.on_after_forgot_password()
def on_after_forgot_password(user: User, token: str, request: Request):
    print(f"User {user.id} has forgot their password. Reset token: {token}")
