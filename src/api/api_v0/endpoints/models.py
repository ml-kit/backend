from io import StringIO
from typing import List, Union

import pandas as pd
from api.api_v0.endpoints.users import fastapi_users_back
from core.aws import S3Helper
from core.dependencies import (
    model_fit_depends,
    my_model_depends,
    own_project_dependency,
)
from core.fields import ObjectIdStr
from core.settings import STAGE
from fastapi import APIRouter, Depends, File, Form, HTTPException, Response, UploadFile
from models.model import CreateModel, Model
from models.model.model_meta import MODELS_INFO
from models.model.tree_interpretation import (
    CatBoostTreeInterpretation,
    SklearnTreeInterpretation,
)
from models.user import User
from pydantic.types import conint
from starlette.background import BackgroundTasks
from starlette.responses import StreamingResponse

router = APIRouter()


@router.get("/meta/?")
async def meta_model_info(
    *, user: User = Depends(fastapi_users_back.get_current_active_user)
):
    return MODELS_INFO


@router.post("/fit/?", response_model=Model)
async def fit_model(*, model: Model = Depends(model_fit_depends)):
    await model.run_fitting()
    return model


@router.get("/?", response_model=List[Model])
async def list_model(
    *, user: User = Depends(fastapi_users_back.get_current_active_user),
):
    models = await Model.filter(project={"$in": user.projects})
    return models


@router.get("/{model_id}/interpretation/decision_tree/?")
async def interpretation_decision_tree(
    *, model: Model = Depends(my_model_depends), tree_id: conint(ge=0) = 1
):
    if not model.algorithm.is_tree_based:
        raise HTTPException(
            status_code=400, detail="Available only for tree based algorithm."
        )
    if model.algorithm.is_sklearn:
        plots = SklearnTreeInterpretation(model, True)
    else:
        plots = CatBoostTreeInterpretation(model, True)
    return Response(content=await plots.get_tree_plot(tree_id), media_type="image/png")


@router.get("/{model_id}/?", response_model=Model)
async def detail_model(
    *, model: Model = Depends(my_model_depends),
):
    return model


@router.delete("/{model_id}/?")
async def delete_model(
    model_id: ObjectIdStr,
    user: User = Depends(fastapi_users_back.get_current_active_user),
):
    obj = await Model.find_one_and_delete(_id=model_id, project={"$in": user.projects})
    S3Helper().delete_file(obj.get_model_path())
    return "OK"


@router.post("/?")
async def create_model(
    *,
    user: User = Depends(fastapi_users_back.get_current_active_user),
    creating: CreateModel,
    background_tasks: BackgroundTasks,
):
    project = await own_project_dependency(creating.project_id, user)
    model = await Model.create_model(
        project, creating.model_method, creating.hyper_params, creating.scaling
    )
    if STAGE == "local":
        background_tasks.add_task(model.run_fitting)
    else:
        background_tasks.add_task(model.send_to_queue)
    return model


@router.post("/{model_id}/predict/?")
async def predict_from_records(
    *, model: Model = Depends(my_model_depends), records: Union[List[dict], dict]
):
    if isinstance(records, dict):
        records = [records]
    try:
        df = pd.DataFrame.from_records(records)
    except Exception as exc:
        raise HTTPException(status_code=400, detail=f"Invalid format data: {str(exc)}")
    model.check_dataframe(df)
    predicts = (await model.predict(df)).tolist()
    if len(predicts) == 1:
        return predicts[0]
    return predicts


@router.post("/{model_id}/predict/csv/")
async def predict_from_csv(
    *,
    model: Model = Depends(my_model_depends),
    index_col_name: str = Form(..., alias="indexColName"),
    file: UploadFile = File(...),
):
    df = pd.read_csv(file.file)
    model.check_dataframe(df)
    predicts = await model.predict(df, index_col=index_col_name)
    f = StringIO(predicts.to_csv(index=False))
    return StreamingResponse(content=f, media_type="text/csv")
