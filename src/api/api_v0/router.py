from fastapi import APIRouter

from .endpoints.common import router as common_router
from .endpoints.models import router as model_router
from .endpoints.projects import router as project_router
from .endpoints.users import router as user_router

router = APIRouter()
router.include_router(user_router, prefix="/users")
router.include_router(common_router)
router.include_router(project_router, prefix="/projects")
router.include_router(model_router, prefix="/models")
