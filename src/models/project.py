from io import BytesIO
from typing import List, Tuple, Union

import numpy as np
import pandas as pd
from core.aws import S3Helper
from core.settings import PROJECT_COLLECTION_NAME, USER_COLLECTION_NAME
from db.mongodb import get_collection
from models.column import BaseStatistics, Column, ColumnFunction
from models.column_train import ColumnToTrain
from models.common import BaseDBModel

S3 = S3Helper()


class Project(BaseDBModel):
    name: str
    owner: str
    dataset: str
    shape: Tuple[int, int]
    columns: List[Column] = []

    class Config(BaseDBModel.Config):
        CLASS_NAME = "Project"
        COLLECTION_NAME: str = PROJECT_COLLECTION_NAME
        schema_extra = {
            "example": {
                "name": "My project",
                "dataset": "dataset.csv",
                "shape": [1000, 6],
                "columns": [],
            }
        }

    def get_path_dataset(self):
        return f"users/{self.owner}/projects/{self.id}/{self.dataset}"

    @classmethod
    def can_be_categories(
        cls, n_unique: Union[int, np.int], length: Union[int, np.int], dtype: str
    ) -> bool:
        if "float" in dtype.lower():
            return False
        return n_unique < 200 and n_unique / length < 0.15

    @classmethod
    async def create(
        cls, user_id: str, project_name: str, file_name: str, file: bytes,
    ):
        df = pd.read_csv(BytesIO(file))

        project = cls(
            name=project_name, owner=user_id, dataset=file_name, shape=df.shape
        )

        n_unique = df.nunique()
        na = df.isna().sum()

        numeric = df.select_dtypes(include="number")
        numeric_cols = set(numeric)

        mean = numeric.mean()
        median = numeric.median()
        mode = numeric.mode().iloc[0]
        mi = numeric.min()
        ma = numeric.max()
        std = numeric.std()

        for col_name, dtype in df.dtypes.iteritems():
            column = Column(
                name=col_name,
                dataType=dtype.name,
                na=int(na[col_name]),
                nunique=int(n_unique[col_name]),
                canBeCategories=cls.can_be_categories(
                    n_unique[col_name], df.shape[0], dtype.name
                ),
            )

            if col_name in numeric_cols:
                base_type = int if "int" in dtype.name else float

                stats = BaseStatistics(
                    mean=float(mean[col_name]),
                    median=base_type(median[col_name]),
                    mode=base_type(mode[col_name]),
                    min=base_type(mi[col_name]),
                    max=base_type(ma[col_name]),
                    std=base_type(std[col_name]),
                )
                column.base_stats = stats
            else:
                column.enabled = column.can_be_categories
                column.categories = column.can_be_categories
                if not column.enabled:
                    column.function = ColumnFunction.DROP

            if column.can_be_categories:
                unique = df[col_name].dropna().unique().tolist()
                column.unique_values = unique

            project.columns.append(column)

        await project.insert_one()

        user_collection = get_collection(USER_COLLECTION_NAME)
        await user_collection.update_one(
            {"id": user_id}, {"$push": {"projects": project.id}}
        )

        return project

    async def get_dataframe(self) -> pd.DataFrame:
        file = S3.download_file(self.get_path_dataset())
        df = pd.read_csv(BytesIO(file))
        return df

    @classmethod
    async def find_one_and_delete(
        cls, raise_not_found=True, **kwargs
    ) -> Config.CLASS_NAME:
        project = await super().find_one_and_delete(raise_not_found, **kwargs)
        path = project.get_path_dataset()
        S3.get_bucket().Object(path).delete()
        return project

    def get_column_to_train(self) -> ColumnToTrain:
        req_cols, cat, num = [], [], []
        na = {}
        target = None
        for column in self.columns:
            if not column.enabled or column.function == ColumnFunction.DROP:
                continue
            if column.function == ColumnFunction.FEATURE:
                req_cols.append(column.name)
                fill_value = column.get_fill_na_value()
                if fill_value:
                    na[column.name] = fill_value
                if column.categories:
                    cat.append(column.name)
                else:
                    num.append(column.name)
            elif column.function == ColumnFunction.TARGET:
                target = column
        res = ColumnToTrain(req_cols=req_cols, cat=cat, num=num, na=na, target=target)
        return res

    def get_column_by_name(self, name: str) -> Column:
        for column in self.columns:
            if column.name == name:
                return column
