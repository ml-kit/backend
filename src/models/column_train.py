from typing import List

from models.column import Column
from pydantic import BaseModel


class ColumnToTrain(BaseModel):
    req_cols: List[str] = list()
    cat: List[str] = list()
    num: List[str] = list()
    na: dict = dict()
    target: Column = None

    def get_columns(self):
        res = self.req_cols.copy()
        if self.target:
            res.append(self.target.name)
        return res
