from typing import List

from core.fields import ObjectIdStr
from fastapi_users import models

from models.common import ConfigObjectId


class User(models.BaseUser):
    projects: List[ObjectIdStr] = []

    class Config(ConfigObjectId):
        pass


class UserCreate(User, models.BaseUserCreate):
    pass


class UserUpdate(User, models.BaseUserUpdate):
    pass


class UserDB(User, models.BaseUserDB):
    pass
