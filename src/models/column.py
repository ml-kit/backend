from enum import Enum
from typing import List, Optional, Union

from pydantic import Field
from pydantic.main import BaseModel
from pydantic.types import constr


class ColumnFunction(str, Enum):
    FEATURE = "feature"
    TARGET = "target"
    DROP = "drop"


class BaseStatistics(BaseModel):
    mean: Optional[Union[int, float]]
    median: Optional[Union[int, float]]
    mode: Union[int, float, str]
    min: Optional[Union[int, float]]
    max: Optional[Union[int, float]]
    std: Optional[Union[int, float]]


class ColumnBase(BaseModel):
    name: constr(min_length=1)
    fill_na: constr(min_length=1) = Field("drop", alias="fillNa")
    categories: bool = False
    function: ColumnFunction = ColumnFunction.FEATURE


class Column(ColumnBase):
    data_type: str = Field(..., alias="dataType")
    na: int
    nunique: int
    base_stats: BaseStatistics = Field(None, alias="baseStats")
    enabled: bool = True
    can_be_categories: bool = Field(True, alias="canBeCategories")
    unique_values: List[Union[str, int, float, bool]] = Field(
        None, alias="uniqueValues"
    )

    def get_fill_na_value(self):
        if self.fill_na == "drop":
            return
        if self.fill_na == "mean":
            return self.base_stats.mean
        elif self.fill_na == "median":
            return self.base_stats.median
        elif self.fill_na == "mode":
            return self.base_stats.mode
        elif self.fill_na == "min":
            return self.base_stats.min
        elif self.fill_na == "max":
            return self.base_stats.max
        elif self.fill_na == "std":
            return self.base_stats.std
        else:
            return self.fill_na
