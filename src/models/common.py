from abc import ABC
from copy import deepcopy
from typing import Callable, List, Optional, Any

from bson import ObjectId
from core.fields import ObjectIdStr
from core.settings import DATABASE_NAME
from db.mongodb import get_database
from fastapi import HTTPException
from motor.motor_asyncio import AsyncIOMotorClient, AsyncIOMotorCollection
from pydantic import BaseModel, Field


class ConfigObjectId:
    allow_population_by_field_name = True
    json_encoders = {ObjectId: lambda x: str(x)}


class BaseDBModel(BaseModel, ABC):
    __slots__ = ('_old_dict',)

    id: Optional[ObjectIdStr] = Field(None, alias="_id")

    class Config(ConfigObjectId):
        CLASS_NAME = "BaseDBModel"
        DB_NAME: str = DATABASE_NAME
        GET_DB: Callable[[], AsyncIOMotorClient] = get_database
        COLLECTION_NAME: str = None

    def _update_dict(self):
        object.__setattr__(self, '_old_dict', deepcopy(self.__dict__))

    def __init__(self, **data: Any) -> None:
        super().__init__(**data)
        self._update_dict()

    @classmethod
    async def get_collection(cls) -> AsyncIOMotorCollection:
        db = cls.Config.GET_DB()
        collection = db[cls.Config.DB_NAME][cls.Config.COLLECTION_NAME]
        return collection

    @classmethod
    async def filter(cls, **kwargs) -> List[Config.CLASS_NAME]:
        collection = await cls.get_collection()
        result = []
        async for document in collection.find(kwargs):
            model = cls(**document)
            result.append(model)
        return result

    @classmethod
    async def _find_one(
        cls, search_kwargs: dict, mode: str = "search", raise_not_found: bool = True
    ) -> Config.CLASS_NAME:
        if isinstance(search_kwargs.get("_id"), str):
            search_kwargs["_id"] = ObjectId(search_kwargs["_id"])

        collection = await cls.get_collection()

        if mode == "search":
            document = await collection.find_one(search_kwargs)
        elif mode == "delete":
            document = await collection.find_one_and_delete(search_kwargs)
        else:
            raise HTTPException(
                status_code=500, detail="Invalid post callback function name"
            )

        if document:
            return cls(**document)

        if raise_not_found:
            raise HTTPException(
                status_code=404, detail=f"{cls.Config.CLASS_NAME} not found."
            )

    @classmethod
    async def find_one(cls, raise_not_found=True, **kwargs) -> Config.CLASS_NAME:
        return await cls._find_one(
            search_kwargs=kwargs, mode="search", raise_not_found=raise_not_found
        )

    @classmethod
    async def find_one_and_delete(
        cls, raise_not_found=True, **kwargs
    ) -> Config.CLASS_NAME:
        return await cls._find_one(
            search_kwargs=kwargs, mode="delete", raise_not_found=raise_not_found
        )

    @classmethod
    async def get_by_id(cls, _id: ObjectIdStr) -> Config.CLASS_NAME:
        return await cls._find_one(search_kwargs={"_id": _id})

    @classmethod
    async def get_own_by_id(cls, _id: ObjectIdStr, user_id: str) -> Config.CLASS_NAME:
        return await cls._find_one(search_kwargs={"_id": _id, "owner": user_id})

    async def insert_one(self):
        collection = await self.get_collection()
        _id = await collection.insert_one(self.dict(exclude={"id"}, by_alias=True))
        self.id = _id.inserted_id
        self._old_dict["id"] = _id.inserted_id

    async def update_model(self, **fields):
        collection = await self.get_collection()
        update_result = await collection.update_one({"_id": self.id}, {'$set': fields})
        self.__dict__.update(fields)
        self._old_dict.update(fields)
        return update_result

    async def update(self) -> bool:
        fields = {}
        for k, v in self.__dict__.items():
            if self._old_dict[k] != v:
                fields[k] = v
        if not fields:
            return False
        collection = await self.get_collection()
        update_result = await collection.update_one({"_id": self.id}, {'$set': fields})
        self._update_dict()
        return True
