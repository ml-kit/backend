from enum import Enum
from typing import List

from pydantic import Field
from pydantic.main import BaseModel
from pydantic.types import constr


class ChartType(str, Enum):
    BAR = "bar"
    HEATMAP = "heatmap"
    DISTRIBUTION = "distribution"
    CATPLOT = "Grouped Categories Plot"


class BaseChartConfig(BaseModel):
    chart_type: ChartType = Field(..., alias="chartType")
    main_column: constr(min_length=1) = Field(..., alias="mainColumn")
    additional_columns: List[constr(min_length=1)] = Field(
        alias="additionalColumns", default_factory=list
    )

    def get_list_columns(self):
        return [self.main_column, *self.additional_columns]
