import json
import pickle
import re
import sys
import traceback
from io import StringIO
from logging import getLogger
from tempfile import NamedTemporaryFile, TemporaryDirectory
from typing import List, Optional, Tuple, Union

import boto3
import numpy as np
import pandas as pd
from core.aws import S3Helper
from core.fields import ObjectIdStr
from core.matplotlib_charts import MatplotlibCharts
from core.settings import (
    AWS_S3_PUBLIC_BUCKET_NAME,
    MODEL_COLLECTION_NAME,
    SQS_QUEUE_MESSAGEGROUPID,
    SQS_QUEUE_REGION,
    TRAINING_SQS_QUEUE,
)
from fastapi import HTTPException
from models.common import BaseDBModel
from models.model.model_method import ModelMethods
from models.model.model_status import ModelStatus
from models.model.model_type import ModelType
from models.project import Project
from pydantic import Field
from sklearn.metrics import f1_score, mean_squared_error, roc_auc_score
from sklearn.model_selection import train_test_split

logger = getLogger(__name__)


class Model(BaseDBModel):
    project: ObjectIdStr
    project_name: str = Field(..., alias="projectName")
    model_type: ModelType = Field(..., alias="modelType")
    algorithm: ModelMethods
    status: ModelStatus
    required_columns: List[str] = Field(..., alias="requiredColumns")
    cat_features: List[str] = Field(..., alias="catFeatures")
    fill_na = Field(dict(), alias="fillNa")
    target: str
    columns_after_pre_processing: List[str] = Field(
        ..., alias="columnsAfterPreProcessing"
    )
    hyper_params: Optional[dict] = Field(dict(), alias="hyperParams")
    metrics: Optional[dict] = dict()
    feature_coefficients: Optional[dict] = dict()
    exception: Optional[str] = str()
    charts: Optional[dict] = dict()
    learning_log: Optional[str] = Field(str(), alias="learningLogs")
    _sqs_task_id: Optional[str] = None
    scaling: Optional[bool] = False
    scale_params: Optional[dict] = dict()
    estimators: Optional[int] = 1

    class Config(BaseDBModel.Config):
        CLASS_NAME = "Model"
        COLLECTION_NAME: str = MODEL_COLLECTION_NAME

    def get_model_dir(self):
        return f"models/{str(self.project)}"

    def get_model_path(self):
        return f"{self.get_model_dir()}/{str(self.id)}.pickle"

    def get_learning_plot_path(self, ext="png"):
        return f"{self.get_model_dir()}/{str(self.id)}_learning.{ext}"

    @staticmethod
    def clean_dataframe(df: pd.DataFrame, columns: list, na: dict):
        df = df[columns]
        df = df.fillna(na)
        df.dropna(inplace=True)
        return df

    @staticmethod
    def one_hot_encoding(df: pd.DataFrame, columns: list):
        return pd.get_dummies(df, columns=columns)

    def get_cols_with_target(self):
        cols = self.required_columns.copy()
        if self.target:
            cols.append(self.target)
        return cols

    @staticmethod
    def get_scales(cols: list, project: Project) -> dict:
        scales = {}
        for col_name in cols:
            for column in project.columns:
                if column.name == col_name:
                    kf = {"mean": column.base_stats.mean, "std": column.base_stats.std}
                    scales[col_name] = kf
                    break
        return scales

    @classmethod
    async def create_model(
        cls,
        project: Project,
        model_method: ModelMethods,
        hyper_params: dict = None,
        scaling: bool = False,
    ) -> Config.CLASS_NAME:
        train = project.get_column_to_train()

        if not model_method.is_clustering and train.target is None:
            raise HTTPException(
                status_code=400, detail="Target column doesn't choosed."
            )

        if model_method.get_model_type() == ModelType.REGRESSION and (
            train.target.categories or train.target.data_type == "object"
        ):
            raise HTTPException(
                status_code=400, detail="Regression available only for numeric target."
            )
        elif (
            model_method.get_model_type() == ModelType.CLASSIFICATION
            and not train.target.categories
        ):
            raise HTTPException(
                status_code=400,
                detail="Classification available only for categorical target.",
            )
        df = await project.get_dataframe()

        df = cls.clean_dataframe(df, train.get_columns(), train.na)

        if model_method.need_one_hot:
            df = cls.one_hot_encoding(df, train.cat)

        x, y = df.drop(columns=train.target.name), df[train.target.name]

        scales, target_scales = {}, {}
        if scaling:
            to_scale = list(set(train.req_cols) - set(train.cat))
            if model_method.get_model_type() == ModelType.REGRESSION:
                to_scale.append(train.target.name)
            scales = cls.get_scales(to_scale, project)

        model = cls(
            project=project.id,
            projectName=project.name,
            modelType=model_method.get_model_type(),
            algorithm=model_method,
            status=ModelStatus.PENDING,
            requiredColumns=train.req_cols,
            catFeatures=train.cat,
            columnsAfterPreProcessing=list(x),
            hyperParams=hyper_params,
            target=train.target.name,
            fillNa=train.na,
            scaling=scaling,
            scale_params=scales,
        )

        await model.insert_one()

        return model

    def scale(self, df: pd.DataFrame):
        scales = self.scale_params
        for col_name in list(df):
            if col_name in scales:
                cfs = scales[col_name]
                if cfs["std"] != 0:
                    df[col_name] -= cfs["mean"]
                    df[col_name] /= cfs["std"]

    def unscale(self, predicts: np.ndarray):
        if self.target in self.scale_params and self.scale_target["std"] != 0:
            predicts *= self.scale_target["std"]
            predicts += self.scale_target["mean"]

    def base_preprocess(self, df: pd.DataFrame) -> pd.DataFrame:
        cols = self.get_cols_with_target()
        df = self.clean_dataframe(df, cols, self.fill_na)
        if self.scaling:
            self.scale(df)
        if self.algorithm.need_one_hot and self.cat_features:
            df = self.one_hot_encoding(df, self.cat_features)
        if self.target:
            return df[[*self.columns_after_pre_processing, self.target]]
        return df[self.columns_after_pre_processing]

    async def get_project(self) -> Project:
        return await Project.get_by_id(self.project)

    async def get_dataframe(self) -> pd.DataFrame:
        project = await self.get_project()
        df = await project.get_dataframe()
        return df

    def preprocess_for_fitting(
        self, df: pd.DataFrame
    ) -> Tuple[pd.DataFrame, Union[pd.DataFrame, pd.Series]]:
        df = self.base_preprocess(df)
        x, y = df.drop(columns=self.target), df[self.target]
        x = x[self.columns_after_pre_processing]
        return x, y

    async def evaluate_model(self, clf, x_test, y_test):
        metrics = {}
        if self.algorithm.is_sklearn:
            if hasattr(clf, "coef_"):
                coefs = clf.coef_.flatten().tolist()
            else:
                coefs = clf.feature_importances_.flatten().tolist()
            features_coefs = {k: v for k, v in zip(list(x_test), coefs)}
        else:
            coefs = clf.get_feature_importance().flatten().tolist()
            features_coefs = {k: v for k, v in zip(clf.feature_names_, coefs)}
        y_pred = clf.predict(x_test)
        if self.model_type == self.model_type.CLASSIFICATION:
            metrics["Acc"] = clf.score(x_test, y_test)
            metrics["Roc-Auc"] = roc_auc_score(y_test, y_pred)
            metrics["F1"] = f1_score(y_test, y_pred)
        elif self.model_type == self.model_type.REGRESSION:
            metrics["R^2"] = clf.score(x_test, y_test)
            metrics["MSE"] = mean_squared_error(y_test, y_pred)
            metrics["RMSE"] = mean_squared_error(y_test, y_pred, squared=False)

        estimators = 1
        if hasattr(clf, "n_estimators"):
            estimators = clf.n_estimators
        elif hasattr(clf, "tree_count_"):
            estimators = clf.tree_count_

        await self.update_model(
            metrics=metrics, feature_coefficients=features_coefs, estimators=estimators
        )

    def save_model(self, clf):
        if self.algorithm.is_sklearn:
            bits = pickle.dumps(clf, protocol=5)
        else:
            with NamedTemporaryFile() as tmp:
                clf.save_model(tmp.name)
                tmp.seek(0)
                bits = tmp.read()
        s3 = S3Helper()
        s3.upload_file(self.get_model_path(), bits)

    def load_model(self):
        s3 = S3Helper()
        bits = s3.download_file(self.get_model_path())
        if self.algorithm.is_sklearn:
            return pickle.loads(bits)
        else:
            clf = self.algorithm.get_model_object()
            with NamedTemporaryFile() as tmp:
                tmp.write(bits)
                tmp.flush()
                tmp.seek(0)
                clf.load_model(tmp.name)
                return clf

    @staticmethod
    def clear_stdout(log: str) -> str:
        return re.sub(r"(\/[^\s]+\/)+.*?\.py:\d*:?\s*", "", log)

    async def run_fitting(self):
        logger.info(
            f"[Project: {self.project_name}] Start fitting model {self.id} with type of {self.algorithm}."
        )
        old_stdout, old_stderr = sys.stdout, sys.stderr
        try:
            df = await self.get_dataframe()
            x, y = self.preprocess_for_fitting(df)

            x_train, x_test, y_train, y_test = train_test_split(
                x, y, test_size=0.15, random_state=42, shuffle=True
            )

            with TemporaryDirectory() as log_dir:
                if self.algorithm.is_cat_boost:
                    self.hyper_params["train_dir"] = log_dir
                clf = self.algorithm.get_model_object(self.hyper_params)

                await self.update_model(status=self.status.TRAINING)

                stdout = StringIO()
                sys.stdout = sys.stderr = stdout
                charts = {}

                if self.algorithm.is_cat_boost:
                    clf.fit(
                        x_train,
                        y_train,
                        cat_features=self.cat_features,
                        eval_set=(x_test, y_test),
                        logging_level="Verbose",
                    )
                    learning_logs = json.load(open(f"{log_dir}/catboost_training.json"))
                    plot_png = MatplotlibCharts().learning_plot(learning_logs)
                    url = S3Helper(bucket_name=AWS_S3_PUBLIC_BUCKET_NAME).upload_public(
                        self.get_learning_plot_path(), plot_png
                    )
                    charts["Learning"] = url
                else:
                    clf.fit(x_train, y_train)

                sys.stdout, sys.stderr = old_stdout, old_stderr
                logs = self.clear_stdout(stdout.getvalue())

                await self.update_model(
                    status=self.status.EVALUATING, learningLogs=logs, charts=charts
                )
            await self.evaluate_model(clf, x_test, y_test)
            self.save_model(clf)
            await self.update_model(status=self.status.COMPLETE, exception="")
            logger.info(
                f"[Project: {self.project_name}] Finished training model {self.id} with type of {self.algorithm}."
            )
        except Exception as exc:
            sys.stdout, sys.stderr = old_stdout, old_stderr
            await self.update_model(
                status=self.status.FAIL,
                exception=f"{str(exc)}\n\n{traceback.format_exc()}",
            )

    def required_columns_for_predicts(self) -> set:
        return set(self.required_columns) - set(self.fill_na.keys())

    @staticmethod
    def fill_cols(df: pd.DataFrame, cols: list, value: Union[int, str, None] = 0):
        for col in cols:
            df[col] = value

    def prepare_df_for_predict(self, df: pd.DataFrame) -> pd.DataFrame:
        none_cols = list(set(self.required_columns) - set(df))
        if none_cols:
            self.fill_cols(df, none_cols, None)
        df = self.clean_dataframe(df, self.required_columns, self.fill_na)
        if self.scaling:
            self.scale(df)
        if self.algorithm.need_one_hot:
            df = self.one_hot_encoding(df, self.cat_features)
        none_cols = list(set(self.columns_after_pre_processing) - set(df))
        if none_cols:
            self.fill_cols(df, none_cols, 0)
        return df[self.columns_after_pre_processing]

    def check_dataframe(self, df: pd.DataFrame):
        not_enough = self.required_columns_for_predicts() - set(df)
        if not_enough:
            raise HTTPException(
                status_code=400, detail=f"Expected: {','.join(map(str, not_enough))}",
            )

    async def predict(
        self, df: pd.DataFrame, index_col=None
    ) -> Union[np.ndarray, pd.DataFrame]:
        if index_col:
            df.index = df[index_col]
        x = self.prepare_df_for_predict(df.copy())
        clf = self.load_model()
        predicts = clf.predict(x)
        if self.algorithm.is_regression and self.scaling:
            self.unscale(predicts)
        if index_col:
            return pd.DataFrame({index_col: x.index, self.target: predicts})
        return predicts

    async def send_to_queue(self):
        sqs_response = boto3.client("sqs", SQS_QUEUE_REGION).send_message(
            QueueUrl=TRAINING_SQS_QUEUE,
            MessageGroupId=SQS_QUEUE_MESSAGEGROUPID,
            MessageBody=json.dumps({"id": str(self.id)}),
        )
        await self.update_model(_sqs_task_id=sqs_response)
