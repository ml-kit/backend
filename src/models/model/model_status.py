from enum import Enum


class ModelStatus(str, Enum):
    NEW = "NEW"
    PENDING = "PENDING"
    TRAINING = "TRAINING"
    EVALUATING = "EVALUATING"
    FAIL = "FAIL"
    COMPLETE = "COMPLETE"
