from enum import Enum

from catboost import CatBoostClassifier
from sklearn.ensemble import RandomForestClassifier

from models.model.model_type import ModelType
from sklearn.linear_model import (
    ElasticNet,
    Lasso,
    LinearRegression,
    LogisticRegression,
    Ridge,
)
from sklearn.tree import DecisionTreeClassifier


class ModelMethods(str, Enum):
    LINEAR_CLASSIFICATION = "linearClassification"
    LINEAR_REGRESSION = "linearRegression"
    LINEAR_REGRESSION_LASSO = "linearRegressionLasso"
    LINEAR_REGRESSION_RIDGE = "linearRegressionRidge"
    LINEAR_REGRESSION_ElasticNet = "linearRegressionElasticNet"
    DecisionTreeClassifier = "DecisionTreeClassifier"
    RandomForestClassifier = "RandomForestClassifier"
    CatBoostClassifier = "CatBoostClassifier"
    DBSCAN = "DBSCAN"

    @classmethod
    def _get_classifications_methods(cls) -> set:
        _CLASSIFICATIONS = {
            cls.LINEAR_CLASSIFICATION,
            cls.DecisionTreeClassifier,
            cls.RandomForestClassifier,
            cls.CatBoostClassifier,
        }
        return _CLASSIFICATIONS

    @classmethod
    def _get_regression_methods(cls) -> set:
        _REGRESSIONS = {
            cls.LINEAR_REGRESSION,
            cls.LINEAR_REGRESSION_LASSO,
            cls.LINEAR_REGRESSION_RIDGE,
            cls.LINEAR_REGRESSION_ElasticNet,
        }
        return _REGRESSIONS

    @classmethod
    def _get_clustering_methods(cls) -> set:
        _CLUSTERING = {cls.DBSCAN}
        return _CLUSTERING

    @classmethod
    def get_scickit_learn_methods(cls) -> set:
        _SKLEARN_MODELS = {
            cls.LINEAR_CLASSIFICATION,
            cls.LINEAR_REGRESSION,
            cls.LINEAR_REGRESSION_LASSO,
            cls.LINEAR_REGRESSION_RIDGE,
            cls.LINEAR_REGRESSION_ElasticNet,
            cls.DecisionTreeClassifier,
            cls.RandomForestClassifier,
            cls.DBSCAN,
        }
        return _SKLEARN_MODELS

    @classmethod
    def get_cat_boost_methods(cls) -> set:
        return {cls.CatBoostClassifier}

    def get_model_type(self) -> ModelType:
        if self in self._get_classifications_methods():
            return ModelType.CLASSIFICATION
        if self in self._get_regression_methods():
            return ModelType.REGRESSION

    @property
    def is_clustering(self) -> bool:
        return self in self._get_clustering_methods()

    @property
    def need_one_hot(self) -> bool:
        return self not in self.get_cat_boost_methods()

    @property
    def is_sklearn(self) -> bool:
        return self in self.get_scickit_learn_methods()

    @property
    def is_regression(self):
        return self in self._get_regression_methods()

    @property
    def is_cat_boost(self) -> bool:
        return self in self.get_cat_boost_methods()

    @property
    def is_tree_based(self) -> bool:
        return self in {
            self.DecisionTreeClassifier,
            self.RandomForestClassifier,
            self.CatBoostClassifier,
        }

    def get_model_object(self, params=None):
        if params is None:
            params = dict()
        if self == self.LINEAR_CLASSIFICATION:
            if "penalty" not in params:
                params["penalty"] = "l2"
            cls = LogisticRegression
            if params.get("penalty") == "l1":
                params["solver"] = "saga"
        elif self == self.LINEAR_REGRESSION:
            cls = LinearRegression
        elif self == self.LINEAR_REGRESSION_LASSO:
            cls = Lasso
        elif self == self.LINEAR_REGRESSION_RIDGE:
            cls = Ridge
        elif self == self.LINEAR_REGRESSION_ElasticNet:
            cls = ElasticNet
        elif self == self.DecisionTreeClassifier:
            cls = DecisionTreeClassifier
        elif self == self.RandomForestClassifier:
            cls = RandomForestClassifier
        elif self == self.CatBoostClassifier:
            cls = CatBoostClassifier
        else:
            raise ValueError(f"{self} does not exist.")
        clf = cls(**params)
        return clf
