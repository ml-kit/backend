from typing import Any

from core.fields import ObjectIdStr
from models.model.model_method import ModelMethods
from pydantic import BaseModel, Field, root_validator


class CreateModel(BaseModel):
    project_id: ObjectIdStr = Field(..., alias="projectId")
    model_method: ModelMethods = Field(..., alias="modelMethod")
    scaling: bool = False
    hyper_params: dict = dict()

    @staticmethod
    def _transform_type(value: str, type_name: str) -> Any:
        if type_name == "float":
            value = value.replace(",", ".")
            return float(value)
        elif type_name == "int":
            return int(value)
        elif type_name == "bool":
            return bool(value)
        elif type_name == "array":
            return value.replace(" ", "").split(",")
        return value

    @root_validator
    def check_hyper_params(cls, values):
        from models.model.model_meta import get_model_info

        meta = get_model_info(values.get("model_method"))["params"]
        hyper = values.get("hyper_params", {})
        for param in hyper:
            if param in meta:
                hyper[param] = cls._transform_type(
                    hyper[param], meta[param]["data_type"]
                )
            else:
                raise ValueError(f"{param} does not available.")
        return values
