from typing import Dict, List, Optional, Union

from models.model import ModelMethods, ModelType
from pydantic import Field
from pydantic.main import BaseModel


class OptionMeta(BaseModel):
    value: Union[str, int, float]
    description: Optional[str] = ""


class ParamMeta(BaseModel):
    description: str
    data_type: str = Field(..., alias="dataType")
    default: Union[str, float, int]
    options: Optional[List[OptionMeta]] = list()


class ModelMeta(BaseModel):
    name: str
    model_type: ModelType = Field(..., alias="modelType")
    display: str
    description: Optional[str] = ""
    params: Dict[str, ParamMeta] = dict()


MODELS_INFO = {
    ModelType.CLASSIFICATION: [
        {
            "name": ModelMethods.LINEAR_CLASSIFICATION,
            "modelType": ModelType.CLASSIFICATION,
            "display": "Linear Classification",
            "description": "Logistic regression, despite its name, is a linear model for classification rather than regression. Logistic regression is also known in the literature as logit regression, maximum-entropy classification (MaxEnt) or the log-linear classifier. In this model, the probabilities describing the possible outcomes of a single trial are modeled using a logistic function.Logistic regression is implemented in LogisticRegression. This implementation can fit binary, One-vs-Rest, or multinomial logistic regression with optional L1, L2 or Elastic-Net regularization.",
            "params": {
                "penalty": {
                    "description": "Used to specify the norm used in the penalization. The ‘newton-cg’, ‘sag’ and ‘lbfgs’ solvers support only l2 penalties. ‘elasticnet’ is only supported by the ‘saga’ solver. If ‘none’ (not supported by the liblinear solver), no regularization is applied.",
                    "data_type": "str",
                    "default": "l2",
                    "options": [
                        {"value": "l1", "description": "Lasso Regularization"},
                        {"value": "l2", "description": "Ridge Regularization"},
                        {
                            "value": "elasticnet",
                            "description": "ElasticNet Regularization",
                        },
                    ],
                },
                "solver": {
                    "description": "Algorithm to use in the optimization problem.",
                    "data_type": "str",
                    "default": "lbfgs",
                    "options": [
                        {
                            "value": "liblinear",
                            "description": "For small datasets, ‘liblinear’ is a good choice, whereas ‘sag’ and ‘saga’ are faster for large ones. 'liblinear' does not support setting penalty='none' and multi-class problem",
                        },
                        {
                            "value": "newton-cg",
                            "description": "Available for multiclass problems and handle L2 penalty or no penalty",
                        },
                        {
                            "value": "lbfgs",
                            "description": "Available for multiclass problems and handle L2 penalty or no penalty",
                        },
                        {
                            "value": "sag",
                            "description": "Available for multiclass problems and handle L2 or no penalty",
                        },
                        {
                            "value": "saga",
                            "description": "Available for multiclass problems and supports l1, L2, elasticnet penalty.",
                        },
                    ],
                },
                "tol": {
                    "description": "Tolerance for stopping criteria",
                    "data_type": "float",
                    "default": "1e-4",
                },
                "C": {
                    "description": "Inverse of regularization strength; must be a positive float. Like in support vector machines, smaller values specify stronger regularization.",
                    "data_type": "float",
                    "default": 1.0,
                },
                "fit_intercept": {
                    "description": "Specifies if a constant (a.k.a. bias or intercept) should be added to the decision function.",
                    "data_type": "bool",
                    "default": True,
                },
                "max_iter": {
                    "description": "Maximum number of iterations taken for the solvers to converge.",
                    "data_type": "int",
                    "default": 100,
                },
            },
        },
        {
            "name": ModelMethods.DecisionTreeClassifier,
            "modelType": ModelType.CLASSIFICATION,
            "display": "Decision Tree",
            "description": "Decision Trees (DTs) are a non-parametric supervised learning method used for classification and regression. The goal is to create a model that predicts the value of a target variable by learning simple decision rules inferred from the data features.",
            "params": {
                "criterion": {
                    "description": "The function to measure the quality of a split. Supported criteria are “gini” for the Gini impurity and “entropy” for the information gain.",
                    "data_type": "str",
                    "default": "gini",
                    "options": [
                        {"value": "gini", "description": ""},
                        {"value": "entropy", "description": ""},
                    ],
                },
                "splitter": {
                    "description": "The strategy used to choose the split at each node. Supported strategies are “best” to choose the best split and “random” to choose the best random split.",
                    "data_type": "str",
                    "default": "best",
                    "options": [
                        {"value": "best", "description": ""},
                        {"value": "random", "description": ""},
                    ],
                },
                "max_depth": {
                    "description": "The maximum depth of the tree. If None, then nodes are expanded until all leaves are pure or until all leaves contain less than min_samples_split samples.",
                    "data_type": "int",
                    "default": "None",
                },
                "min_samples_split": {
                    "description": "The minimum number of samples required to split an internal node:",
                    "data_type": "int",
                    "default": 2,
                },
                "min_samples_leaf": {
                    "description": "The minimum number of samples required to be at a leaf node. A split point at any depth will only be considered if it leaves at least min_samples_leaf training samples in each of the left and right branches. This may have the effect of smoothing the model, especially in regression.",
                    "data_type": "int",
                    "default": 1,
                },
            },
        },
        {
            "name": ModelMethods.RandomForestClassifier,
            "modelType": ModelType.CLASSIFICATION,
            "display": "Random Forest Classifier",
            "description": "A random forest is a meta estimator that fits a number of decision tree classifiers on various sub-samples of the dataset and uses averaging to improve the predictive accuracy and control over-fitting. The sub-sample size is controlled with the max_samples parameter if bootstrap=True (default), otherwise the whole dataset is used to build each tree.",
            "params": {
                "n_estimators": {
                    "description": "The number of trees in the forest.",
                    "data_type": "int",
                    "default": 100,
                },
                "criterion": {
                    "description": "The function to measure the quality of a split. Supported criteria are “gini” for the Gini impurity and “entropy” for the information gain.",
                    "data_type": "str",
                    "default": "gini",
                    "options": [
                        {"value": "gini", "description": ""},
                        {"value": "entropy", "description": ""},
                    ],
                },
                "max_leaf_nodes": {
                    "description": "Grow trees with max_leaf_nodes in best-first fashion. Best nodes are defined as relative reduction in impurity. If None then unlimited number of leaf nodes.",
                    "data_type": "int",
                    "default": "None",
                },
                "max_depth": {
                    "description": "The maximum depth of the tree. If None, then nodes are expanded until all leaves are pure or until all leaves contain less than min_samples_split samples.",
                    "data_type": "int",
                    "default": "None",
                },
                "min_samples_split": {
                    "description": "The minimum number of samples required to split an internal node:",
                    "data_type": "int",
                    "default": 2,
                },
                "min_samples_leaf": {
                    "description": "The minimum number of samples required to be at a leaf node. A split point at any depth will only be considered if it leaves at least min_samples_leaf training samples in each of the left and right branches. This may have the effect of smoothing the model, especially in regression.",
                    "data_type": "int",
                    "default": 1,
                },
            },
        },
        {
            "name": ModelMethods.CatBoostClassifier,
            "modelType": ModelType.CLASSIFICATION,
            "display": "CatBoost Classifier (Boosted Trees)",
            "description": "CatBoost is a machine learning algorithm that uses gradient boosting on decision trees",
            "params": {
                "loss_function": {
                    "description": "The metric to use in training. The specified value also determines the machine learning problem to solve.",
                    "data_type": "str",
                    "default": "Logloss",
                    "options": [
                        {"value": "Logloss", "description": ""},
                        {"value": "CrossEntropy", "description": ""},
                        {"value": "Lq", "description": ""},
                        {"value": "RMSE", "description": ""},
                        {"value": "MultiRMSE", "description": ""},
                        {"value": "MultiClass", "description": ""},
                        {"value": "MultiClassOneVsAll", "description": ""},
                        {"value": "MAPE", "description": ""},
                        {"value": "Poisson", "description": ""},
                        {"value": "PairLogit", "description": ""},
                        {"value": "QueryRMSE", "description": ""},
                        {"value": "QueryRMSE", "description": ""},
                    ],
                },
                "eval_metric": {
                    "description": "The metric used for overfitting detection and best model selection",
                    "data_type": "str",
                    "default": "Logloss",
                    "options": [
                        {"value": "RMSE", "description": ""},
                        {"value": "Logloss", "description": ""},
                        {"value": "MAE", "description": ""},
                        {"value": "CrossEntropy", "description": ""},
                        {"value": "Quantile", "description": ""},
                        {"value": "LogLinQuantile", "description": ""},
                        {"value": "Lq", "description": ""},
                        {"value": "MultiRMSE", "description": ""},
                        {"value": "MultiClass", "description": ""},
                        {"value": "MultiClassOneVsAll", "description": ""},
                        {"value": "MAPE", "description": ""},
                        {"value": "Poisson", "description": ""},
                        {"value": "PairLogit", "description": ""},
                        {"value": "PairLogitPairwise", "description": ""},
                        {"value": "QueryRMSE", "description": ""},
                        {"value": "QuerySoftMax", "description": ""},
                        {"value": "Tweedie", "description": ""},
                        {"value": "SMAPE", "description": ""},
                        {"value": "Recall", "description": ""},
                        {"value": "Precision", "description": ""},
                        {"value": "F1", "description": ""},
                        {"value": "TotalF1", "description": ""},
                        {"value": "Accuracy", "description": ""},
                        {"value": "BalancedAccuracy", "description": ""},
                        {"value": "BalancedErrorRate", "description": ""},
                        {"value": "Kappa", "description": ""},
                        {"value": "WKappa", "description": ""},
                        {"value": "LogLikelihoodOfPrediction", "description": ""},
                        {"value": "AUC", "description": ""},
                        {"value": "R2", "description": ""},
                        {"value": "FairLoss", "description": ""},
                        {"value": "NumErrors", "description": ""},
                        {"value": "MCC", "description": ""},
                        {"value": "BrierScore", "description": ""},
                        {"value": "HingeLoss", "description": ""},
                        {"value": "HammingLoss", "description": ""},
                        {"value": "ZeroOneLoss", "description": ""},
                        {"value": "MSLE", "description": ""},
                        {"value": "MedianAbsoluteError", "description": ""},
                        {"value": "Huber", "description": ""},
                        {"value": "Expectile", "description": ""},
                        {"value": "MultiRMSE", "description": ""},
                        {"value": "PairAccuracy", "description": ""},
                        {"value": "AverageGain", "description": ""},
                        {"value": "PFound", "description": ""},
                        {"value": "NDCG", "description": ""},
                        {"value": "DCG", "description": ""},
                        {"value": "FilteredDCG", "description": ""},
                        {"value": "NormalizedGini", "description": ""},
                        {"value": "PrecisionAt", "description": ""},
                        {"value": "RecallAt", "description": ""},
                        {"value": "MAP", "description": ""},
                    ],
                },
                "custom_metric": {
                    "description": "Metric values to output during training. These functions are not optimized and are displayed for informational purposes only. Read by comma separator.",
                    "data_type": "array",
                    "default": "Logloss",
                    "options": [
                        {"value": "RMSE", "description": ""},
                        {"value": "Logloss", "description": ""},
                        {"value": "MAE", "description": ""},
                        {"value": "CrossEntropy", "description": ""},
                        {"value": "Quantile", "description": ""},
                        {"value": "LogLinQuantile", "description": ""},
                        {"value": "Lq", "description": ""},
                        {"value": "MultiRMSE", "description": ""},
                        {"value": "MultiClass", "description": ""},
                        {"value": "MultiClassOneVsAll", "description": ""},
                        {"value": "MAPE", "description": ""},
                        {"value": "Poisson", "description": ""},
                        {"value": "PairLogit", "description": ""},
                        {"value": "PairLogitPairwise", "description": ""},
                        {"value": "QueryRMSE", "description": ""},
                        {"value": "QuerySoftMax", "description": ""},
                        {"value": "Tweedie", "description": ""},
                        {"value": "SMAPE", "description": ""},
                        {"value": "Recall", "description": ""},
                        {"value": "Precision", "description": ""},
                        {"value": "F1", "description": ""},
                        {"value": "TotalF1", "description": ""},
                        {"value": "Accuracy", "description": ""},
                        {"value": "BalancedAccuracy", "description": ""},
                        {"value": "BalancedErrorRate", "description": ""},
                        {"value": "Kappa", "description": ""},
                        {"value": "WKappa", "description": ""},
                        {"value": "LogLikelihoodOfPrediction", "description": ""},
                        {"value": "AUC", "description": ""},
                        {"value": "R2", "description": ""},
                        {"value": "FairLoss", "description": ""},
                        {"value": "NumErrors", "description": ""},
                        {"value": "MCC", "description": ""},
                        {"value": "BrierScore", "description": ""},
                        {"value": "HingeLoss", "description": ""},
                        {"value": "HammingLoss", "description": ""},
                        {"value": "ZeroOneLoss", "description": ""},
                        {"value": "MSLE", "description": ""},
                        {"value": "MedianAbsoluteError", "description": ""},
                        {"value": "Huber", "description": ""},
                        {"value": "Expectile", "description": ""},
                        {"value": "MultiRMSE", "description": ""},
                        {"value": "PairAccuracy", "description": ""},
                        {"value": "AverageGain", "description": ""},
                        {"value": "PFound", "description": ""},
                        {"value": "NDCG", "description": ""},
                        {"value": "DCG", "description": ""},
                        {"value": "FilteredDCG", "description": ""},
                        {"value": "NormalizedGini", "description": ""},
                        {"value": "PrecisionAt", "description": ""},
                        {"value": "RecallAt", "description": ""},
                        {"value": "MAP", "description": ""},
                        {"value": "CtrFactor", "description": ""},
                    ],
                },
                "learning_rate": {
                    "description": "The learning rate. Used for reducing the gradient step.",
                    "data_type": "float",
                    "default": "0.03",
                },
                "l2_leaf_reg": {
                    "description": "Coefficient at the L2 regularization term of the cost function. Any positive value is allowed.",
                    "data_type": "float",
                    "default": 3.0,
                },
                "max_depth": {
                    "description": "Depth of the tree.",
                    "data_type": "int",
                    "default": 6,
                },
                "min_data_in_leaf": {
                    "description": "The minimum number of training samples in a leaf. CatBoost does not search for new splits in leaves with samples count less than the specified value. Can be used only with the Lossguide and Depthwise growing policies.",
                    "data_type": "int",
                    "default": 1,
                }
            },
        },
    ],
    ModelType.REGRESSION: [
        {
            "name": ModelMethods.LINEAR_REGRESSION,
            "modelType": ModelType.REGRESSION,
            "display": "Linear Regression",
            "description": "LinearRegression fits a linear model with coefficients w = (w1, …, wp) to minimize the residual sum of squares between the observed targets in the dataset, and the targets predicted by the linear approximation.",
            "params": {
                "normalize": {
                    "description": "If True, the regressors X will be normalized before regression by subtracting the mean and dividing by the l2-norm.",
                    "data_type": "bool",
                    "default": False,
                },
            },
        },
        {
            "name": ModelMethods.LINEAR_REGRESSION_RIDGE,
            "modelType": ModelType.REGRESSION,
            "display": "Linear Regression Ridge",
            "description": "This model solves a regression model where the loss function is the linear least squares function and regularization is given by the l2-norm. Also known as Ridge Regression or Tikhonov regularization.",
            "params": {
                "alpha": {
                    "description": "Regularization strength; must be a positive float. Regularization improves the conditioning of the problem and reduces the variance of the estimates. Larger values specify stronger regularization. Alpha corresponds to 1 / (2C) in other linear models such as LogisticRegression",
                    "data_type": "float",
                    "default": 1.0,
                },
                "max_iter": {
                    "description": "Maximum number of iterations for conjugate gradient solver. For ‘sparse_cg’ and ‘lsqr’ solvers, the default value is determined by scipy.sparse.linalg. For ‘sag’ solver, the default value is 1000.",
                    "data_type": "int",
                    "default": "None",
                },
                "tol": {
                    "description": "Precision of the solution.",
                    "data_type": "float",
                    "default": "1e-3",
                },
                "solver": {
                    "description": "Solver to use in the computational routines:",
                    "data_type": "str",
                    "default": "auto",
                    "options": [
                        {
                            "value": "auto",
                            "description": "‘auto’ chooses the solver automatically based on the type of data.",
                        },
                        {
                            "value": "svd",
                            "description": "‘svd’ uses a Singular Value Decomposition of X to compute the Ridge coefficients. More stable for singular matrices than ‘cholesky’.",
                        },
                        {
                            "value": "cholesky",
                            "description": "‘cholesky’ uses the standard scipy.linalg.solve function to obtain a closed-form solution.",
                        },
                        {
                            "value": "sparse_cg",
                            "description": "‘sparse_cg’ uses the conjugate gradient solver as found in scipy.sparse.linalg.cg. As an iterative algorithm, this solver is more appropriate than ‘cholesky’ for large-scale data (possibility to set tol and max_iter).",
                        },
                        {
                            "value": "lsqr",
                            "description": "‘lsqr’ uses the dedicated regularized least-squares routine scipy.sparse.linalg.lsqr. It is the fastest and uses an iterative procedure.",
                        },
                        {
                            "value": "sag",
                            "description": "‘sag’ uses a Stochastic Average Gradient descent, and ‘saga’ uses its improved, unbiased version named SAGA. Both methods also use an iterative procedure, and are often faster than other solvers when both n_samples and n_features are large. Note that ‘sag’ and ‘saga’ fast convergence is only guaranteed on features with approximately the same scale. You can preprocess the data with a scaler from sklearn.preprocessing.",
                        },
                    ],
                },
            },
        },
        {
            "name": ModelMethods.LINEAR_REGRESSION_LASSO,
            "modelType": ModelType.REGRESSION,
            "display": "Linear Regression Lasso",
            "description": "Linear Model trained with L1 prior as regularizer (aka the Lasso).The optimization objective for Lasso is: (1 / (2 * n_samples)) * ||y - Xw||^2_2 + alpha * ||w||_1. Technically the Lasso model is optimizing the same objective function as the Elastic Net with l1_ratio=1.0 (no L2 penalty).",
            "params": {
                "alpha": {
                    "description": "Constant that multiplies the L1 term. Defaults to 1.0. alpha = 0 is equivalent to an ordinary least square, solved by the LinearRegression object. ",
                    "data_type": "float",
                    "default": 1.0,
                },
                "max_iter": {
                    "description": "The maximum number of iterations",
                    "data_type": "int",
                    "default": 1000,
                },
                "tol": {
                    "description": "Precision of the solution.",
                    "data_type": "float",
                    "default": "1e-4",
                },
                "selection": {
                    "description": "If set to ‘random’, a random coefficient is updated every iteration rather than looping over features sequentially by default. This (setting to ‘random’) often leads to significantly faster convergence especially when tol is higher than 1e-4.",
                    "data_type": "str",
                    "default": "cyclic",
                    "options": [
                        {"value": "cyclic", "description": ""},
                        {"value": "random", "description": ""},
                    ],
                },
            },
        },
        {
            "name": ModelMethods.LINEAR_REGRESSION_ElasticNet,
            "modelType": ModelType.REGRESSION,
            "display": "Linear Regression ElasticNet",
            "description": "Linear regression with combined L1 and L2 priors as regularizer. The parameter l1_ratio corresponds to alpha in the glmnet R package while alpha corresponds to the lambda parameter in glmnet. Specifically, l1_ratio = 1 is the lasso penalty. Currently, l1_ratio <= 0.01 is not reliable, unless you supply your own sequence of alpha.",
            "params": {
                "alpha": {
                    "description": "Constant that multiplies the penalty terms. Defaults to 1.0. See the notes for the exact mathematical meaning of this parameter. alpha = 0 is equivalent to an ordinary least square, solved by the LinearRegression object.",
                    "data_type": "float",
                    "default": 1.0,
                },
                "l1_ratio": {
                    "description": "The ElasticNet mixing parameter, with 0 <= l1_ratio <= 1. For l1_ratio = 0 the penalty is an L2 penalty. For l1_ratio = 1 it is an L1 penalty. For 0 < l1_ratio < 1, the penalty is a combination of L1 and L2.",
                    "data_type": "float",
                    "default": 0.5,
                },
                "max_iter": {
                    "description": "The maximum number of iterations",
                    "data_type": "int",
                    "default": 1000,
                },
                "tol": {
                    "description": "The tolerance for the optimization: if the updates are smaller than tol, the optimization code checks the dual gap for optimality and continues until it is smaller than tol.",
                    "data_type": "float",
                    "default": "1e-4",
                },
                "selection": {
                    "description": "If set to ‘random’, a random coefficient is updated every iteration rather than looping over features sequentially by default. This (setting to ‘random’) often leads to significantly faster convergence especially when tol is higher than 1e-4.",
                    "data_type": "str",
                    "default": "cyclic",
                    "options": [
                        {"value": "cyclic", "description": ""},
                        {"value": "random", "description": ""},
                    ],
                },
            },
        },
    ],
}


def get_model_info(method: ModelMethods):
    models = MODELS_INFO[method.get_model_type()]
    for model in models:
        if model["name"] == method:
            return model
    raise ValueError("Method doesn't exist in meta info.")
