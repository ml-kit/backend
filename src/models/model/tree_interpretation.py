import base64
from abc import ABC

from catboost import Pool
from fastapi import HTTPException
from graphviz import Source
from models.model import Model
from sklearn.tree import export_graphviz


class BaseTreeInterpretation(ABC):
    def _graph_to_png(self, graph: str):
        png_bytes = Source(graph).pipe(format="png")
        if self.use_base64:
            png_bytes = base64.standard_b64encode(png_bytes)
        return png_bytes


class SklearnTreeInterpretation(BaseTreeInterpretation):
    def __init__(self, model: Model, use_base64=True):
        self.model = model
        self.clf = self.model.load_model()
        self.use_base64 = use_base64
        if hasattr(self.clf, "estimators_"):
            self.estimators = self.clf.estimators_
        else:
            self.estimators = [self.clf]
        self.class_names = None

    async def load_class_names(self):
        project = await self.model.get_project()
        values = project.get_column_by_name(self.model.target).unique_values
        class_names = [f"{self.model.target}: {name}" for name in values]
        self.class_names = class_names

    def _get_graph(self, num: int) -> str:
        clf = self.estimators[num]
        graph = export_graphviz(
            clf,
            feature_names=self.model.columns_after_pre_processing,
            filled=True,
            class_names=self.class_names,
        )
        return graph

    async def get_tree_plot(self, num: int = 0) -> bytes:
        if num >= len(self.estimators):
            raise HTTPException(
                status_code=400,
                detail=f"Index out of range. Index available: 0...{len(self.estimators)}.",
            )
        if self.class_names is None:
            await self.load_class_names()
        graph = self._get_graph(num)
        png_bytes = self._graph_to_png(graph)
        return png_bytes


class CatBoostTreeInterpretation(BaseTreeInterpretation):
    def __init__(self, model: Model, use_base64=True):
        self.model = model
        self.clf = self.model.load_model()
        self.use_base64 = use_base64
        self.pool = None

    async def load_pool(self):
        df = await self.model.get_dataframe()
        x, y = self.model.preprocess_for_fitting(df)
        self.pool = Pool(
            x, y, cat_features=self.model.cat_features, feature_names=list(x),
        )

    async def get_tree_plot(self, num: int = 0) -> bytes:
        if num >= self.clf.tree_count_:
            raise HTTPException(
                status_code=400,
                detail=f"Index out of range. Index available: 0...{self.clf.tree_count_}.",
            )
        if self.pool is None:
            await self.load_pool()
        graph = self.clf.plot_tree(num, pool=self.pool)
        png_bytes = self._graph_to_png(graph)
        return png_bytes
