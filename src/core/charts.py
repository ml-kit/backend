from typing import Tuple

import numpy as np
import pandas as pd
from fastapi import HTTPException
from models.charts import BaseChartConfig
from models.project import Project
from sklearn.preprocessing import LabelEncoder


def convert_column_to_intervals(series: pd.Series, n_bins: int = 10) -> pd.Series:
    bins = np.linspace(series.min(), series.max(), n_bins)
    return pd.cut(series, bins)


def convert_or_raise_error(
    series: pd.Series, n_bins: int = 10, cat_lim: int = 20
) -> Tuple[pd.Series, bool]:
    if series.nunique() > cat_lim:
        if np.issubdtype(series.dtype, np.int) or np.issubdtype(series.dtype, np.float):
            return convert_column_to_intervals(series, n_bins), True
        raise HTTPException(
            status_code=400,
            detail=f"{series.name} is not numeric and "
            f"have too many unique values to be a category",
        )
    return series, False


def convert_intervals_to_string(intervals, prefix: str = "") -> list:
    res = list(map(lambda border: f"{prefix}{border.left} - {border.right}", intervals))
    return res


async def preprocess_bar(project: Project, chart_config: BaseChartConfig) -> dict:
    cols = chart_config.get_list_columns()
    if len(set(cols)) != 2:
        raise HTTPException(
            status_code=400, detail="Must be choosed 2 unique columns for bar chart."
        )

    cols = [chart_config.main_column, *chart_config.additional_columns]
    df = await project.get_dataframe()

    df = df[cols].dropna()

    col1, col2, = (convert_or_raise_error(df[col])[0] for col in cols)

    groups = df.groupby([col1, col2]).size().unstack(fill_value=0)

    if isinstance(col2.dtype, pd.CategoricalDtype):
        groups.columns = convert_intervals_to_string(groups.columns, prefix=cols[1])
    else:
        groups.columns = f"{cols[1]}: " + groups.columns.astype(str)

    if isinstance(col1.dtype, pd.CategoricalDtype):
        groups.index = convert_intervals_to_string(groups.index)

    groups[cols[0]] = groups.index.values.astype(str)

    res = groups.to_dict("records")
    return {
        "data": res,
        "keys": groups.columns.tolist(),
        "indexBy": chart_config.main_column,
    }


async def preprocess_heatmap(project: Project, chart_config: BaseChartConfig) -> dict:
    cols = chart_config.get_list_columns()
    if len(set(cols)) < 2:
        raise HTTPException(
            status_code=400,
            detail="Must be choosed more than one unique column for heat map.",
        )
    df = await project.get_dataframe()
    df = df[cols].dropna()
    cats = list(df.select_dtypes("object"))
    for cat in cats:
        encoder = LabelEncoder()
        df[cat] = encoder.fit_transform(df[cat])
    corr = df.corr()
    corr["index"] = corr.index
    data = corr.round(3).to_dict("records")
    return {"data": data, "keys": corr["index"].tolist(), "indexBy": "index"}
