import os
import traceback
from io import BytesIO
from logging import getLogger

import boto3
from core.settings import AWS_S3_BUCKET_NAME, STAGE, USE_CACHE

logger = getLogger(__name__)


class S3Helper:
    def __init__(self, bucket_name=AWS_S3_BUCKET_NAME, use_cache=USE_CACHE):
        self.bucket_name = bucket_name
        self.use_cache = use_cache

    def get_bucket(self):
        s3 = boto3.resource("s3")
        return s3.Bucket(self.bucket_name)

    def upload_file(self, path: str, file: bytes):
        os_path = os.path.join("cache", path)
        if self.use_cache:
            os.makedirs(os.path.dirname(os_path), exist_ok=True)
            with open(os_path, "wb") as f:
                f.write(file)
        if STAGE != "local":
            bucket = self.get_bucket()
            return bucket.put_object(Key=path, Body=file)

    def get_public_url(self, path):
        bucket_location = boto3.client("s3").get_bucket_location(
            Bucket=self.bucket_name
        )
        object_url = "https://s3-{0}.amazonaws.com/{1}/{2}".format(
            bucket_location["LocationConstraint"], self.bucket_name, path
        )
        return object_url

    def upload_public(self, path: str, file: bytes):
        bucket = self.get_bucket()
        resp = bucket.put_object(Key=path, Body=file, ACL="public-read")
        # resp = bucket.put_object_acl(acl = "public-read", key=path)
        return self.get_public_url(path)

    def download_file(self, path: str) -> bytes:
        os_path = os.path.join("cache", path)

        if self.use_cache and os.path.exists(os_path):
            os.makedirs(os.path.dirname(os_path), exist_ok=True)
            with open(os_path, "rb") as file:
                return file.read()
        if STAGE == "local":
            raise Exception("Not found!")
        bucket = self.get_bucket()
        with BytesIO() as f:
            bucket.download_fileobj(path, f)
            file_bytes = f.getbuffer().tobytes()

        if self.use_cache:
            os.makedirs(os.path.dirname(os_path), exist_ok=True)
            with open(os_path, "wb") as file:
                file.write(file_bytes)
        return file_bytes

    def delete_file(self, path: str) -> None:
        if STAGE != "local":
            bucket = self.get_bucket()
            try:
                bucket.delete_key(path)
            except Exception as exc:
                logger.warning(
                    f"Failed to delete: {path}\nException: {str(exc)}\n{traceback.format_exc()}"
                )
        cache_path = os.path.join("cache", path)
        if os.path.exists(cache_path):
            os.remove(cache_path)
