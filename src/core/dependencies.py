from typing import List

from api.api_v0.endpoints.users import fastapi_users_back
from core.fields import ObjectIdStr
from fastapi import Depends, HTTPException
from models.column import ColumnBase
from models.model import Model
from models.project import Project
from models.user import User
from pydantic import BaseModel, Field


async def own_project_dependency(
    project_id: ObjectIdStr,
    user: User = Depends(fastapi_users_back.get_current_active_user),
) -> Project:
    project = await Project.get_own_by_id(project_id, user.id)
    return project


async def exist_column_names(
    columns: List[ColumnBase], project: Project = Depends(own_project_dependency)
) -> Project:
    cols_available = set(map(lambda p: p.name, project.columns))
    cols_name = set(map(lambda p: p.name, columns))
    invalid_names = cols_name - cols_available
    if invalid_names:
        cols_str = ", ".join(map(lambda s: f"'{s}'", invalid_names))
        raise HTTPException(
            status_code=404, detail=f"Column names not exist: {cols_str}."
        )
    return project


class FitModel(BaseModel):
    model_id: ObjectIdStr = Field(..., alias="id")


async def model_fit_depends(model_fit: FitModel) -> Model:
    model = await Model.get_by_id(model_fit.model_id)
    return model


async def my_model_depends(
    model_id: ObjectIdStr,
    user: User = Depends(fastapi_users_back.get_current_active_user),
) -> Model:
    model = await Model.find_one(_id=model_id, project={"$in": user.projects})
    return model
