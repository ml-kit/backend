from bson import ObjectId


class ObjectIdStr(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not ObjectId.is_valid(str(v)):
            raise ValueError(f"Not a valid ObjectId: {v}")
        return ObjectId(str(v))

    def __iter__(self):
        yield ObjectId(self.__str__())
