import os
import uvloop

from fastapi_users.authentication import JWTAuthentication

##################################Set UVLOOP as default#################################
uvloop.install()
########################################################################################

PROJECT_NAME = os.environ.get("PROJECT_NAME", "ML-KIT")
DEBUG = os.environ.get("DEBUG", "").lower() == "true"
USE_CACHE = os.environ.get("USE_CACHE", "True").lower() == "true"
SECRET = os.environ.get("SECRET", "SECRET")
STAGE = os.environ.get("STAGE", "prod")
BASE_PATH = os.path.dirname(os.path.dirname(__file__))
SSL_KEY_PATH_NAME = os.environ.get("SSL_KEY_PATH_NAME", "ssl.txt")
SSL_URL = os.environ.get("SSL_URL", "/.well-known/9CF4BC6FC866EC11CA95B8874D0B28B8.txt")

##################################Allow Hosts###########################################
ALLOWED_HOSTS = os.getenv("ALLOWED_HOSTS", "").split(",")
ALL_METHODS = ("DELETE", "GET", "OPTIONS", "PATCH", "POST", "PUT")
if not ALLOWED_HOSTS:
    ALLOWED_HOSTS = ["*"]
########################################################################################

##################################Database##############################################
DATABASE_URL = os.environ.get("DATABASE_URL", "mongodb://mongo:27017")
DATABASE_NAME = os.environ.get("DATABASE_NAME", "ml-kit")
MAX_CONNECTIONS_COUNT = int(os.environ.get("MAX_CONNECTIONS_COUNT", 100))
MIN_CONNECTIONS_COUNT = int(os.environ.get("MIN_CONNECTIONS_COUNT", 0))
########################################################################################

##################################Collections###########################################
USER_COLLECTION_NAME = os.environ.get("USER_COLLECTION_NAME", "users")
PROJECT_COLLECTION_NAME = os.environ.get("PROJECT_COLLECTION_NAME", "projects")
MODEL_COLLECTION_NAME = os.environ.get("MODEL_COLLECTION_NAME", "models")
########################################################################################

##################################Auth Backend##########################################
AUTH_BACKENDS = [
    JWTAuthentication(secret=SECRET, lifetime_seconds=24 * 3600),
]
########################################################################################

AWS_S3_BUCKET_NAME = os.environ.get("AWS_S3_BUCKET_NAME", "ml-kit-dev")
AWS_S3_PUBLIC_BUCKET_NAME = os.environ.get(
    "AWS_S3_PUBLIC_BUCKET_NAME", "ml-kit-dev-public"
)
TRAINING_SQS_QUEUE = os.environ.get("TRAINING_SQS_QUEUE")
SQS_QUEUE_REGION = os.environ.get("SQS_QUEUE_REGION", "eu-central-1")
SQS_QUEUE_MESSAGEGROUPID = os.environ.get(
    "SQS_QUEUE_MESSAGEGROUPID", "5ecec1c24da07299f972f8bc"
)
