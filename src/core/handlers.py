from typing import Callable, Awaitable

from fastapi import FastAPI

from api.api_v0.endpoints.common import health_check_ok, ssl_certificate
from core.settings import SSL_URL
from db.mongodb_utils import connect_to_mongo, close_mongo_connection


def on_startup(app: FastAPI) -> Callable[[None], Awaitable[None]]:
    async def setup():
        await connect_to_mongo()
        from api.api_v0.router import router
        app.include_router(router, prefix="/api/v0")
        app.add_api_route("/", health_check_ok, methods=["GET"])
        app.add_api_route(SSL_URL, ssl_certificate, methods=["GET"])

    return setup


def on_shutdown(app: FastAPI) -> Callable[[None], Awaitable[None]]:
    async def close_app():
        await close_mongo_connection()

    return close_app
