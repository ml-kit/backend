import base64
from io import BytesIO

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from fastapi import HTTPException
from models.charts import BaseChartConfig
from models.project import Project


class MatplotlibCharts:
    def __init__(self, style="white", palette="muted", color_codes=True):
        sns.set(style=style, palette=palette, color_codes=color_codes)

    @classmethod
    def _get_png_plot(cls, plot, use_base64=True) -> bytes:
        fig = plot
        if hasattr(plot, "get_figure"):
            fig = plot.get_figure()

        with BytesIO() as tmp_file:
            fig.savefig(tmp_file, transparent=True, format="png")
            data = tmp_file.getbuffer().tobytes()

        plt.close("all")
        if use_base64:
            data = base64.standard_b64encode(data)
        return data

    async def distribution_chart(self, project: Project, chart_config: BaseChartConfig):
        for column in project.columns:
            if column.name == chart_config.main_column and column.data_type == "object":
                raise HTTPException(
                    status_code=400, detail="Must be choosed only one numeric column."
                )

        df = await project.get_dataframe()
        plot = sns.distplot(df[chart_config.main_column])
        data = self._get_png_plot(plot)
        return data

    async def cat_plot(self, project: Project, chart_config: BaseChartConfig):
        input_columns = chart_config.get_list_columns()
        if len(set(input_columns)) != 3:
            raise HTTPException(
                status_code=400, detail="Must be choosed 3 unique columns.",
            )
        cols = {}
        for column_name, alias in zip(input_columns, ["x", "y", "hue"]):
            for column in project.columns:
                if column_name == column.name:
                    cols[alias] = column
                    break

        if cols["x"].data_type == "object" and cols["y"].data_type == "object":
            raise HTTPException(
                status_code=400, detail="'X' or 'Y' column must be numeric.",
            )
        if cols["x"].nunique > 20 or cols["hue"].nunique > 20:
            raise HTTPException(
                status_code=400,
                detail="Unique values of 'X' and 'Hue' columns must be less then 20.",
            )
        df = await project.get_dataframe()
        plot = sns.catplot(
            x=cols["x"].name,
            y=cols["y"].name,
            hue=cols["hue"].name,
            data=df,
            height=6,
            kind="bar",
        )
        data = self._get_png_plot(plot)
        return data

    @staticmethod
    def __get_metric_names(metrics, prefix: str):
        return list(map(lambda x: prefix + x["name"], metrics))

    def learning_plot(self, logs: dict) -> bytes:
        col_train_names = self.__get_metric_names(
            logs["meta"]["learn_metrics"], prefix="Train: "
        )
        col_test_names = self.__get_metric_names(
            logs["meta"]["test_metrics"], prefix="Test: "
        )
        df = pd.DataFrame.from_records(logs["iterations"])
        train = np.stack(df["learn"].values)
        test = np.stack(df["test"].values)
        train = pd.DataFrame(train, columns=col_train_names)
        test = pd.DataFrame(test, columns=col_test_names)
        df = pd.concat([train, test, df["iteration"]], axis=1)
        df = pd.melt(df, ["iteration"])
        plt.figure(figsize=(15, 10))
        plot = sns.lineplot(x="iteration", y="value", hue="variable", data=df)
        data = self._get_png_plot(plot, use_base64=False)
        return data
